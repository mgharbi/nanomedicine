require 'test_helper'

class NewsTest < ActiveSupport::TestCase
  test "the truth" do
    cecile_prize = news(:cecile_prize)
    assert_equal cecile_prize.title, "Cecile got a prize"
  end
end
