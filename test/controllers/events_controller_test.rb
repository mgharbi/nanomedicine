require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  test "should get index" do
    get "index"
    assert_response :success
    assert_not_nil assigns(:events), "list of events should be populated"
    assert_template "index"
  end

  test "should get show" do
    get "show", id:1
    assert_response :success
    assert_not_nil assigns(:event), "selected event should exist"
    assert_template "show"
  end
end
