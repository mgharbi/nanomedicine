class DeviseCreateDefaultMember < ActiveRecord::Migration
  def migrate(direction)
    super
    # Create a default user
    Member.create!(email:'admin@example.com',
                   password: 'password',
                   password_confirmation: 'password',
                   firstname: 'admin',
                   title: 'undefined',
                   lastname: 'admin',
                   admin: true ) if direction == :up

  end
end
