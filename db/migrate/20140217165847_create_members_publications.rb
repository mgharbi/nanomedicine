class CreateMembersPublications < ActiveRecord::Migration
  def change
    create_table :members_publications, id: false do |t|
      t.integer :member_id
      t.integer :publication_id
    end
  end
end
