class AddDeleteImageToEvent < ActiveRecord::Migration
  def change
    add_column :events, :delete_image, :boolean
  end
end
