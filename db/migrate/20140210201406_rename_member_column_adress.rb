class RenameMemberColumnAdress < ActiveRecord::Migration
  def change
    rename_column :members, :adress, :address
  end
end
