class AddPhoneToMember < ActiveRecord::Migration
  def change
    add_column :members, :phone, :string, null:false, default: ""
  end
end
