class AddDisplayToPublication < ActiveRecord::Migration
  def change
    add_column :publications, :display, :boolean, null: false, default: true
  end
end
