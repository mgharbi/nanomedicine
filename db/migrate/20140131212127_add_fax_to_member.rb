class AddFaxToMember < ActiveRecord::Migration
  def change
    add_column :members, :fax, :string, null: false, default: ""
  end
end
