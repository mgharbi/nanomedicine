class AddTitleDescriptionUrlPlaceStartEndToEvent < ActiveRecord::Migration
  def change
    add_column :events, :title, :string
    add_column :events, :description, :text
    add_column :events, :url, :string
    add_column :events, :place, :string
    add_column :events, :start, :date
    add_column :events, :end, :date
  end
end
