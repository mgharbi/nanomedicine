class AddTitleAuthorsBibPdfToPublication < ActiveRecord::Migration
  def change
    add_column :publications, :title, :string, null: false, default: ""
    add_column :publications, :authors, :string, null: false, default: ""
    add_column :publications, :bib, :text, default: "", null: false
    add_column :publications, :pdf, :string, null:false, default: ""
  end
end
