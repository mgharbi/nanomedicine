class RemovePdfFromPublication < ActiveRecord::Migration
  def change
    remove_column :publications, :pdf
  end
end
