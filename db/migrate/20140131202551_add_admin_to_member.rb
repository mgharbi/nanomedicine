class AddAdminToMember < ActiveRecord::Migration
  def change
    add_column :members, :admin, :boolean, default: false, null: false
  end
end
