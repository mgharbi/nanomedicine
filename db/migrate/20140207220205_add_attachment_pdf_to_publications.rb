class AddAttachmentPdfToPublications < ActiveRecord::Migration
  def self.up
    change_table :publications do |t|
      t.attachment :pdf
    end
  end

  def self.down
    drop_attached_file :publications, :pdf
  end
end
