class RemoveDeleteImageToEvent < ActiveRecord::Migration
  def up
    remove_column :events, :delete_image
  end

  def down
    add_column :events, :delete_image, :boolean
  end
end
