class AddFirstNameLastNameToMember < ActiveRecord::Migration
  def change
    add_column :members, :firstname, :string, null: false, default: ""
    add_column :members, :lastname, :string, null: false, default: ""
  end
end
