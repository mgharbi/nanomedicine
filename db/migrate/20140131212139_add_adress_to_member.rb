class AddAdressToMember < ActiveRecord::Migration
  def change
    add_column :members, :adress, :string, null: false, default: ""
  end
end
