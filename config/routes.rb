Nanomedicine::Application.routes.draw do

  scope "(:locale)", locale: /en|fr/ do
    resources :news, only: [:index, :show] do
      get 'page/:page', :action => :index, :on => :collection
    end
    resources :events, only: [:index, :show] do
      get 'page/:page', :action => :index, :on => :collection
    end
    resources :members, only: [:index, :show] do
      get 'page/:page', :action => :index, :on => :collection
      get ':id/page/:page', :action => :show, :on => :collection
    end
    resources :publications, only: [:index, :show] do
      get 'page/:page', :action => :index, :on => :collection
    end

    get "home/index"
    get "home/contact"
    get "search/results"

    root :to => 'home#index'

    # Admin
    devise_for :members, ActiveAdmin::Devise.config
    ActiveAdmin.routes(self)
  end




  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
