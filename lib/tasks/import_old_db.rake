namespace :db do
  desc "Fill database with data from the old online nanomedicine website"
  task :import_old_db => :environment do
    File.open("#{Rails.root}/db/fixtures/old_member_symfony.yml", 'r') do |file|
      puts "Inserting members with default password"
       YAML::load(file).each do |r|
         case r["status_id"]
         when 87
           status = "phd_student"
         when 88
           status = "msc_student"
         when 89
           status = "research_engineer"
         when 90
           status = "assistant_engineer"
         when 91
           status = "full_professor"
         when 92
           status = "medical_doctor"
         when 93
           status = "assistant_professor"
         else
           status = "undefined"
         end
         m = Member.create!({
           email:       r["email"],
           firstname:   r["first_name"],
           lastname:    r["last_name"],
           phone:       r["phone"] || "",
           bio:         r["bio"] || "",
           fax:         r["fax"] || "",
           address:     r["adress"] || "",
           id: r["id"],
           title:       status,
           password:    "nanomedicine",
         }, 
         without_protection: true)
      end
    end
    File.open("#{Rails.root}/db/fixtures/old_publi_symfony.yml", 'r') do |file|
      puts "Inserting publications"
       YAML::load(file).each do |r|
         att = {}
         att[:authors] = r["author"]
         att[:title]   = r["title"]
         att[:year]    = r["year"]
         att[:member_ids] = [r["member_id"]]
         p = Publication.create_or_update_from_attrs(att)
      end
    end
    File.open("#{Rails.root}/db/fixtures/old_news_symfony.yml", 'r') do |file|
      puts "Inserting news"
       YAML::load(file).each do |r|
         n = News.create!(
           title:   r["title"],
           content: r["content"],
           date:    r["created_at"]
         )
      end
    end
  end
end
