class Member < ActiveRecord::Base
  has_and_belongs_to_many :publications, uniq: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable, :registerable

  TITLE_OPTIONS = [
    "phd_student",
    "msc_student",
    "research_engineer",
    "assistant_engineer",
    "assistant_professor",
    "full_professor",
    "medical_doctor",
    "undefined",
  ]
  validates_inclusion_of :title, :in => TITLE_OPTIONS

  before_save :destroy_picture?

  attr_accessor :delete_picture

  has_attached_file :picture,
                    styles: {medium: "150x150>"},
                    url: "/images/members/:id/:basename.:extension"

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, presence: true
  validates :picture, attachment_content_type: { content_type: /\Aimage\/.*\Z/ }

  private 

  def destroy_picture?
    if delete_picture == "1"
      picture.clear
    end
  end
end
