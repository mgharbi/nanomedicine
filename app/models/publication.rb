require 'bibtex'

class PublicationValidator < ActiveModel::Validator
  def validate(publication)
    bib = BibTeX.parse(publication.bib)
    if bib.count > 1
      publication.errors[:bib] << 'Please input zero or a single bib entry'
    end
    if bib.count == 0
      if publication.title.empty?
        publication.errors[:title] << "Can't be blank"
      end
      if publication.authors.empty?
        publication.errors[:authors] << "Can't be blank"
      end
    else
      if publication.title.empty?
        publication.title = bib[0].title.to_s
      end
      publication.year  = bib[0].year.to_i
      if publication.authors.empty?
        publication.add_authors(bib[0].authors)
      end
      # unless publication.title.empty?
      #   publication.errors[:title] << "This information will be parsed from the .bib"
      # end
      # unless publication.authors.empty?
      #   publication.errors[:authors] << "This information will be parsed from the .bib"
      # end
    end
    if publication.member_ids.empty?
      publication.errors[:members] << "Can't be empty"
    end

    if publication.duplicate?
      publication.errors[:publication] << "A duplicate exists"
    end
  end
end

class Publication < ActiveRecord::Base
  paginates_per 20
  has_and_belongs_to_many :members, uniq: true
  accepts_nested_attributes_for :members 

  attr_accessor :delete_pdf

  has_attached_file :pdf, default_url: ""

  # Cleanup on delete
  before_validation { pdf.clean if delete_pdf == '1' }
  # after_validation :parse_bib

  validates :pdf, attachment_content_type: {content_type:"application/pdf"}
  # validates :member_ids, presence: true
  validates_with PublicationValidator

  # Scopes for easy filtering
  scope :title, ->(title) { where("title like ?", "%#{title}%") }
  scope :year_min, ->(year) { where("year >= ?", year) }
  scope :year_max, ->(year) { where("year <= ?", year) }
  scope :author, ->(author) { where("authors like ?", "%#{author}%") }
  
  # Usefull in controller and view. 
  def self.get_filters
    return [:title,:year_min,:year_max,:author]
  end

  def title=(t)
    t = t.to_s
    if /\{.*\}/.match(t)
      t = t[1..-2]
    end
    write_attribute(:title,t)
  end

  def year=(y)
    y = y.to_i
    write_attribute(:year,y)
  end

  def add_authors(authors)
    for i in 0...authors.length
      unless self.authors.empty?
        self.authors += " and "
      end
      a = authors[i].to_s.force_encoding("UTF-8")
      name = a.split(',')
      name.map! {|elt| elt.strip}
      member = Member.find(:first, conditions: ["lower(lastname) = ?", name[0].downcase])
      if not member.nil? and member.firstname.downcase[0] == name[1].downcase[0]
        self.members << member
      end
      self.authors += a
    end
  end

  def self.create_or_update_from_attrs(attributes) 
    p = Publication.find_or_create_by_title(attributes)
    p.member_ids |= attributes[:member_ids]
    return p
  end

  def duplicate?
    return self.class.exists?(
      title: self.title,
      year: self.year,
      authors: self.authors
    )
  end
end

