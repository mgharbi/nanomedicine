class News < ActiveRecord::Base
  before_save :destroy_image?

  attr_accessor :delete_image

  has_attached_file :image,
                    styles: {medium: "150x150>"},
                    url: "/images/news/:id/:basename.:extension"
  validates :image, attachment_content_type: { content_type: /\Aimage\/.*\Z/ }
  validates :title, presence: true
  validates :content, presence: true
  validates :date, presence: true

  private

  def destroy_image?
    if delete_image == "1"
      image.clear
    end
  end
end
