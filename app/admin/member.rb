ActiveAdmin.register Member do     
  menu :label => proc{ current_member.admin? ? "Members" : "My Profile" }
  # menu :if => proc{ current_member.admin? }

  controller do
    def index
      if !current_member.admin?
        redirect_to action: :show, id: current_member.id
      else
        index!
      end
    end

    def update
      if params[:member][:password].blank?
        params[:member].delete(:password)
        params[:member].delete(:password_confirmation)
      end
      super
    end

    def permitted_params
      params.permit member: [ 
        :email, :password, :password_confirmation, :admin,
        :remember_me, :firstname, :lastname,
        :title, :phone, :fax, :address, :bio, :picture,
        :delete_picture, :publication_ids
      ]
    end
  end

  member_action :change_role do
    if current_member.admin? and resource != current_member
      resource.assign_attributes({admin: !resource.admin})
      resource.save
      flash[:notice] = "#{resource.firstname} #{resource.admin? ? "has been promoted" : "is no more"} admin"
      redirect_to action: :index 
    end
  end

  index download_links: false do                            
    if current_member.admin?
      selectable_column
      column :email                     
      column :firstname
      column :lastname
      column :last_sign_in_at           
      column :sign_in_count             
      column :admin do |member|
        if can? :change_role, member 
          if member.admin?
            link_to "Yes",action: :change_role,id: member.id, method: :put
          else
            link_to "No",action: :change_role,id: member.id, method: :put
          end
        end
      end
    end
    actions
  end                                 

  filter :email                       
  filter :firstname
  filter :lastname

  show title: proc{|m| "#{m.firstname} #{m.lastname}"} do 
    attributes_table do
      row :firstname
      row :lastname
      row :title
      row :email
      row :phone
      row :fax
      row :address
      row :bio  do
         simple_format member.bio
      end
    end
  end

  form do |f|                         
    f.inputs "Admin Details" do       
      f.input :firstname
      f.input :lastname
      f.input :title, as: :select, collection: Member::TITLE_OPTIONS
      f.input :email                  
      f.input :phone
      f.input :fax
      f.input :address
      f.input :bio
      # if f.object.new_record?
      f.input :password               
      f.input :password_confirmation  
      # end
      # if can? :assign_roles, f.object
      #   f.input :admin
      # end
    end                               
    f.inputs "Attachments", multipart: true do
      f.input :picture, as: :file, hint: (f.template.image_tag(f.object.picture.url) if f.object.picture?)
      f.input :delete_picture, label: "Delete Picture", as: :boolean
    end
    f.actions                         
  end                                 
end                                   
