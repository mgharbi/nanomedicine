ActiveAdmin.register Event do

  controller do
    def permitted_params
      params.permit event: [:title, :description, :url, :place, :start, :end, :image, :delete_image]
    end
  end

  index download_links: false do                            
    selectable_column
    column :title                     
    column :place
    column :url
    column :start
    actions
  end                                 
  show title: proc{|m| "#{m.title}"} do 
    attributes_table do
      row :title
      row :description
      row :place
      row :url
      row :start
      row :end
    end
  end
  form do |f|                         
    if f.object.new_record?
      f.object.start = Date.today
      f.object.end   = Date.today
    end
    f.inputs "Event Details" do       
      f.input :title
      f.input :description
      f.input :url
      f.input :place
      f.input :start, label: "Start date"
      f.input :end, label: "End date"
    end                               
    f.inputs "Attachments", multipart: true do
      f.input :image, as: :file, hint: (f.template.image_tag(f.object.image.url) if f.object.image?)
      f.input :delete_image, label: "Delete Image", as: :boolean
    end
    f.actions                         
  end                                 

  filter :title
  filter :start
  filter :place
  filter :url
end
