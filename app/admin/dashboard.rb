ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  action_item("home") do
    link_to "View Site", "/"
  end

  sidebar :managed_data do
    if current_member.admin?
      ul do
        li "#{Member.all.count} members"
        li "#{Publication.all.count} publications"
        li "#{Event.all.count} events"
        li "#{News.all.count} news"
      end
    else
        li "#{current_member.publications.count} publications"
    end
  end

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    para "Welcome to Nanomedicine Laboratory's administration interface!"
  end # content
end
