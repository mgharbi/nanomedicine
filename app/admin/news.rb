ActiveAdmin.register News do

  controller do
    def permitted_params
      params.permit news: [ :title, :content, :image, :delete_image, :date ]
    end
  end

  index download_links: false do                            
      selectable_column
      column :title                     
      column :date             
    actions
  end                                 
  show title: proc{|m| "#{m.title}"} do 
    attributes_table do
      row :title
      row :content
      row :date
    end
  end
  form do |f|                         
    if f.object.new_record?
      f.object.date = Date.today
    end
    f.inputs "News Details" do       
      f.input :title
      f.input :content
      f.input :date
    end                               
    f.inputs "Attachments", multipart: true do
      f.input :image, label: "Image"
      # f.input :delete_image, label: "Delete Picture", as: :boolean
    end
    f.actions                         
  end                                 

  filter :title
  filter :date
end
