require 'bibtex'
ActiveAdmin.register Publication do

  menu :label => proc{ current_member.admin? ? "Publications" : "My Publications" }

  controller do 
    def create
      @publication=Publication.create_or_update_from_attrs(params[:publication])
      flash[:notice] = "#{resource.title} already exists, ownership has been updated"
      create!
    end

    def permitted_params
      params.permit publication: [ 
        :title, :authors, :year, :url, :bib,
        :pdf, :delete_pdf, :display, :member_ids
      ]
    end
  end

  filter :members, collection: proc{ Member.all.collect {|m| [m.firstname+" "+m.lastname, m.id]} }
  filter :authors
  filter :title
  filter :year

  action_item "bibtex", :only => :index do
    link_to('Upload BibTeX', action: :upload_bibtex)
  end

  collection_action :upload_bibtex do
  end

  collection_action :uploaded, method: :post do
    upload_content = params[:bibtex].read
    bib = BibTeX.parse(upload_content)
    n_saved = 0
    n_dup = 0
    bib.each do |b|
      p = Publication.new
      if b.title.nil? or b.title.empty?
        next
      end
      p.title = b.title
      p.add_authors(b.authors)
      if p.members.empty?
        p.members << current_member
      end
      p.year = b.year
      if p.save
        n_saved += 1
      else
        if p.duplicate_exists?
          n_dup += 1
        end
      end
    end
    s = "#{bib.count} entries: #{n_saved} publications saved"
    if n_dup > 0
      s += ", #{n_dup} duplicates ignored."
    end
    flash[:notice] = s

    redirect_to action: :index , notice: 'Bib uploaded!'
  end

  index download_links: false do
    selectable_column
    # TODO make a bib exporter
    column :title, sortable: :title do |publi|
      truncate(publi.title, length:40)
    end
    column :authors, sortable: :authors do |publi|
      truncate(publi.authors, length: 40)
    end
    column :year
    column :owner, sortable: false do |publi|
      publi.members.first.firstname
    end
    column :pdf do |publi|
      if publi.pdf.exists?
        "yes"
      else
        "no"
      end
    end
    actions
  end

  form :html => { :multipart => true } do |f| 
    # if f.object.new_record?
    #   f.object.members << current_member
    # end
    f.semantic_errors :publication
    f.inputs "Data" do 
      f.input :title
      f.input :authors
      f.input :year
      f.input :bib, label: "BibTeX"
      if current_member.admin?
        f.input :members, as: :check_boxes,
                        member_label: lambda { |i| i.firstname+" "+i.lastname }
      end
      f.input :display, as: :boolean, label: "Display on site"
    end
    f.inputs "Attachments", multipart: true do
      f.input :pdf, label: "PDF"
      f.input :delete_pdf, label: "Delete PDF", as: :boolean
    end
    f.actions
  end

  show title: proc{|p| truncate(p.title, length: 40)} do
    attributes_table do
      row :title
      row :authors
      row :members do
        s = ""
        publication.members.each do |m|
          unless s.empty?
            s += " and "
          end
          s += m.firstname
        end
        simple_format s
      end
      row :year
      row :bib do
        simple_format publication.bib
      end
      row :pdf do
        publication.pdf.exists? ? "yes" : "no"
      end
      row :display
    end
  end

end
