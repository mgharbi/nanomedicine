class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_locale

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { locale: I18n.locale }
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def current_ability
    @current_ability ||= Ability.new(current_member)
  end

  def access_denied(exception)
    redirect_to root_url, :alert => exception.message
  end

end
