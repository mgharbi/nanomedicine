class MembersController < ApplicationController
  def index
    @members = Member.order(:lastname).page(params[:page]).per(10)
  end

  def show
    @m = Member.find(params[:id])
    @publications = Kaminari.paginate_array(@m.publications).page(params[:page]).per(10)
  end
end
