class NewsController < ApplicationController
  def index
    @news = News.order(:date).page(params[:page])
  end
  def show
    @n = News.find(params[:id])
  end
end
