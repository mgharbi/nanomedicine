class SearchController < ApplicationController
  def results
    @query = params[:search]
    if not @query.nil? || @query.empty?
      @members = Member.search(lastname_or_firstname_cont: @query).result.order(:lastname)
      @publications = Publication.search(title_cont: @query).result.order(:title)
      @n = @members.count + @publications.count
    else
      @n=0
    end
  end
end
