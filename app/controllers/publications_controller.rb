class PublicationsController < ApplicationController
  
  def index 
    @publications = Publication.order("year DESC").page(params[:page]).per(10)
    # Filter - makes use of scopes
    filtering_params(params).each do |key, value|
      @publications = @publications.public_send(key, value) if value.present?
    end
  end

  def show
    @publication = Publication.find(params[:id])
    render layout: false
  end

  # Parse filtering parameters from url
  private
  def filtering_params(params)
    filters = Publication.get_filters
    params.slice(*filters)
  end

end
