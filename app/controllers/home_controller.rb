class HomeController < ApplicationController
  def index
    @news = News.all
  end

  def contact
    @secretary = Member.where(firstname: "Anne-Marie").first()
  end
end
